import { Component, Input, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { GridsterConfig, GridsterItem, GridsterItemComponent } from 'angular-gridster2';
import * as Highcharts from 'highcharts';
// import * as Exporting from 'highcharts/modules/exporting';
declare let require: any;

const Exporting = require('highcharts/modules/exporting');

Exporting(Highcharts);


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
  export class DashboardComponent implements OnInit {
  @ViewChild("gridsterItem") gridItem: GridsterItemComponent;
  @Input() public item: GridsterItem;
  @Input() public unitHeight: number;
    metricName: string;
    isDarkTheme: boolean;
    chart: any;
    historicalChartOptions: any;
    chartData: any;
    public item1: GridsterItem;
    public item2: GridsterItem;
    public item3: GridsterItem;
    public item4: GridsterItem;

    options: GridsterConfig;
    dashboard: Array<GridsterItem>;
  loaded: boolean;
  
    static itemChange(item: any, itemComponent: any) {
      console.log('itemChanged', item, itemComponent);
    }
  
    static itemResize(item: any, itemComponent: any) {
      console.log('itemResized', item, itemComponent);
    }
  	constructor(
    ) {
      this.unitHeight = 0;
      this.item1 = { x: 0, y: 0, rows: 2, cols: 2 };
      this.item2 = { x: 3, y: 3, rows: 2, cols: 2 };
      this.item3 = { x: 5, y: 2, rows: 2, cols: 3 };
      this.item4 = { x: 3, y: 4, rows: 3, cols: 4 };
      this.historicalChartOptions = {
        chart: {
          zoomType: 'x',
          marginRight: 30
        },
        time: {
          useUTC: false
        },
        title: {
          text: 'Graph values for Metric X'
        },
        credits: {
          enabled: false
        },
        subtitle: {
          text: ''
        },
        exporting: { enabled: true },
        xAxis: {
          type: 'datetime',
          title: {
            text: 'Date / Time'
          }
        },
        yAxis: [
          {
            lineWidth: 1,
            title: {
              text: "Metric X"
            }
          }
        ],
        series: [
          {
            name: "Metric X",
            color: 'rgb(31, 119, 180)',
            data: [
          Math.floor(Math.random() * 10),
          Math.floor(Math.random() * 10),
          Math.floor(Math.random() * 10),
          Math.floor(Math.random() * 10),
          Math.floor(Math.random() * 10),
          Math.floor(Math.random() * 10),
          Math.floor(Math.random() * 10),
          Math.floor(Math.random() * 10),
          Math.floor(Math.random() * 10)
            ]
          }
        ]
      };
    }

    public ngAfterViewInit(): void {
    this.chart = Highcharts.chart('historicalValueChart', this.historicalChartOptions);
      this.loaded = true;
  }
    public ngOnChanges(changes: SimpleChanges) {
      if (this.loaded) {
        this.resizeChart();
      }
    }
  
  
  
    public resizeChart(): void {
        console.log("resizeChart");
        this.historicalChartOptions.chart.height = this.item.rows * (this.unitHeight - 10) + ((this.item.rows - 4) * 10);
        this.historicalChartOptions.chart.width = this.item.cols * (this.unitHeight - 10) + ((this.item.cols - 4) * 10);
  
      if (this.chart.ref) {
          this.chart.ref.setSize(this.historicalChartOptions.chart.width, this.historicalChartOptions.chart.height, false);
      }
    }
    ngOnInit() {

      this.dashboard = [
        { cols: 1, rows: 3, y: 0, x: 0 },
        { cols: 1, rows: 6, y: 0, x: 0 },
        { cols: 2, rows: 4, y: 0, x: 0 },
        { cols: 1, rows: 4, y: 0, x: 0 },
        { cols: 1, rows: 6, y: 0, x: 0 },
        { cols: 2, rows: 5, y: 0, x: 0 },
        { cols: 1, rows: 5, y: 0, x: 0 },
        { cols: 1, rows: 3, y: 0, x: 0 },

      ];
      this.options = {
        itemChangeCallback: DashboardComponent.itemChange,
        itemResizeCallback: DashboardComponent.itemResize,
        displayGrid: 'none',
        maxCols: 3,
        // maxRows: 20,
        draggable: {
          enabled: true, // enables to drag element
          // dropOverItems: true, // nothing changes actually
        },
        pushItems: true,
        // disableScrollHorizontal: true, // can drag only to top or bottom
        resizable: { enabled: true }, // enables to resize element
      };
      setTimeout(() => {
        console.log('Chart loaded');
		this.chart = Highcharts.chart('historicalValueChart', this.historicalChartOptions);
        
      }, 3000);

    }
  
    changedOptions() {
      console.log('changedOptions');
      // this.options.api.optionsChanged();
    }
  
    removeItem(e: any, item: any) {
      this.dashboard.splice(this.dashboard.indexOf(item), 1);
    }
  
    addItem() {
      this.dashboard.push(
        { cols: 1, rows: 3, y: 0, x: 0 }
      );
    }
  
    minimizeItem(e: any, item: { rows: number; }) {
      item.rows = 1;
      // this.options.api.optionsChanged(); // MIKEY NOTE: this makes things going in Gridster2!
    }
  

  

  }
  
