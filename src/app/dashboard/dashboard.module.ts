import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GridsterModule } from 'angular-gridster2';
import { DashboardComponent } from './dashboard.component'
@NgModule({
	imports: [
        CommonModule,
        GridsterModule
	],
	declarations: [
		DashboardComponent
	],
	exports: [
	]
})
export class DashboardModule { }
